package pages.executors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pages.locators.Task6PageLocators;

/**
 * Created by mturon on 19.09.2017.
 */
public class Task6Page {

    private Task6PageLocators locators;

    public Task6Page (WebDriver driver){
        locators = new Task6PageLocators();
        PageFactory.initElements(driver, locators);
    }

    public Task6Page enterLogin(String login){
        locators.loginInput.sendKeys(login);
        return this;
    }

    public Task6Page enterPassword(String password){
        locators.passwordInput.sendKeys(password);
        return this;
    }

    public void clickLoginButton(){
        locators.loginButton.click();
    }

    public void clickDownloadButton(){
        locators.downloadLinkButton.click();
    }

    public boolean isWrongDataAlertDisplayed(){
        return locators.alertBox.isDisplayed();
    }

}
