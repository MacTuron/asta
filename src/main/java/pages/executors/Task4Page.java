package pages.executors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import pages.locators.Task4PageLocators;
import pages.locators.Task6PageLocators;

/**
 * Created by mturon on 19.09.2017.
 */
public class Task4Page {

    private Task4PageLocators locators;
    private WebDriver driver;

    public Task4Page(WebDriver driver) {
        locators = new Task4PageLocators();
        PageFactory.initElements(driver, locators);
    }

    public void clickApplyButton() {
        locators.applyButton.click();
    }

    public Task4Page enterFullName(String name) {
        locators.fullNameInput.sendKeys(name);
        return this;
    }

    public Task4Page enterEmail(String email) {
        locators.emailInput.sendKeys(email);
        return this;
    }

    public Task4Page enterPhoneNumber(String number) {
        locators.phoneNumberInput.sendKeys(number);
        return this;
    }

    public void clickSaveButton() {
        locators.saveButton.click();
    }


}
