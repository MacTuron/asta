package pages.executors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pages.locators.Task3PageLocators;
import pages.locators.Task4PageLocators;

/**
 * Created by mturon on 21.09.2017.
 */
public class Task3Page {

    private Task3PageLocators locators;
    private WebDriver driver;

    public Task3Page(WebDriver driver) {
        locators = new Task3PageLocators();
        PageFactory.initElements(driver, locators);
    }


}
