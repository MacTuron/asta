package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

/**
 * Created by mturon on 21.09.2017.
 */
public class Task1PageLocators {

    @FindBy(how = How.XPATH, using = "//button[text()='Dodaj']")
    public List<WebElement> addButtonList;

    @FindBy(how = How.XPATH, using = "//input[@class='form-control']")
    public List<WebElement> enterQuantityList;

    @FindBy(how = How.XPATH, using = "//div[@class='col-md-12 basket-summary']/p/span[@class='summary-quantity']")
    public WebElement totalQuantity;

    @FindBy(how = How.XPATH, using = "//div[@class='col-md-12 basket-summary']/p/span[@class='summary-price']")
    public WebElement totalPrice;

    @FindBy(how = How.XPATH, using = "//div[@class='row row-in-basket']/div[@class='col-md-9 text-on-button-level']")
    public List<WebElement> itemNameAndPriceList;

    @FindBy(how = How.XPATH, using = "//span[@class='input-group-btn']/button[text()='Usuń']")
    public List<WebElement> deleteButtonList;

    @FindBy(how = How.XPATH, using = "//div[@class='col-md-9 text-on-button-level']/span[@class='row-in-basket-quantity']")
    public List<WebElement> quantityInBasketList;
















}
