package pages.locators;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by mturon on 19.09.2017.
 */
public class Task4PageLocators {

    @FindBy(how = How.XPATH, using = "//div[@class='col-md-3']/button")
    public WebElement applyButton;

    @FindBy(how = How.XPATH, using = "//input[@name='name']")
    public WebElement fullNameInput;

    @FindBy(how = How.XPATH, using = "//input[@name='email']")
    public WebElement emailInput;

    @FindBy(how = How.XPATH, using = "//input[@name='phone']")
    public WebElement phoneNumberInput;

    @FindBy(how = How.ID, using = "save-btn")
    public WebElement saveButton;


}
