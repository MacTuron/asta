package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by mturon on 21.09.2017.
 */
public class Task3PageLocators {

    @FindBy(how = How.XPATH, using = "//*[@class='dropdown-toggle menu-border']")
    public WebElement menuDropdownButton;

    @FindBy(how = How.XPATH, using = "//*[@class='dropdown-toggle menu-border']")
    public WebElement menuDropdownButton;
}
