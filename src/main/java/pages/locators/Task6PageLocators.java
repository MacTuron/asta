package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by mturon on 19.09.2017.
 */
public class Task6PageLocators {

    @FindBy(how = How.ID, using = "LoginForm__username")
    public WebElement loginInput;

    @FindBy(how = How.ID, using = "LoginForm__password")
    public WebElement passwordInput;

    @FindBy(how = How.ID, using = "LoginForm_save")
    public WebElement loginButton;

    @FindBy(how = How.CSS, using = "a[href='/task_6/download']")
    public WebElement downloadLinkButton;

    @FindBy(how = How.CSS, using = ".list-unstyled>li")
    public WebElement alertBox;

}
