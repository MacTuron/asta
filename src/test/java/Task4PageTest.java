import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.executors.Task4Page;

import java.util.Set;


/**
 * Created by mturon on 19.09.2017.
 */
public class Task4PageTest {
    private WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://testingcup.pgs-soft.com/task_4");
    }

    @Test
    public void applyForJob() {
        Task4Page task4Page = new Task4Page(driver);
        task4Page.clickApplyButton();
        task4Page.enterFullName("Maciej Turon");
        task4Page.enterEmail("mac@gg.com");
        task4Page.enterPhoneNumber("888999555");
        task4Page.clickSaveButton();
    }
}
