import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.executors.Task6Page;

import static org.junit.Assert.assertTrue;

/**
 * Created by mturon on 19.09.2017.
 */
public class Task6PageTest {

    private WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://testingcup.pgs-soft.com/task_4");
    }

    @Test
    public void fillValidCredentials() {
        Task6Page task6Page = new Task6Page(driver);
        task6Page.enterLogin("tester");
        task6Page.enterPassword("123-xyz");
        task6Page.clickLoginButton();
        task6Page.clickDownloadButton();

    }

    @Test
    public void fillInvalidCredentials() {
        Task6Page task6Page = new Task6Page(driver);
        task6Page.enterLogin("Maciej");
        task6Page.enterPassword("Haslo1234");
        task6Page.clickLoginButton();
        assertTrue("'Nieprawidłowe dane logowania' was not displayed", task6Page.isWrongDataAlertDisplayed());

    }

}
